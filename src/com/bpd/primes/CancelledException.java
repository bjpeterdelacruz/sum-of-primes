package com.bpd.primes;

/**
 * An exception that is thrown when a user cancels the current calculation.
 * 
 * @author BJ Peter DeLaCruz
 */
public class CancelledException extends Exception {

  private static final long serialVersionUID = 1L;

  /**
   * Creates a new CancelledException instance.
   */
  public CancelledException() {
    super(Constants.CALCULATION_CANCELLED);
  }

}
