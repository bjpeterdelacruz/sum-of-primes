package com.bpd.primes;

import com.bpd.primes.PrimeFinder.Result;

/**
 * A listener that will update components with information stored in a Result object.
 * 
 * @author BJ Peter DeLaCruz
 */
public interface ResultListener {

  /**
   * Updates the components with the information stored in the given Result object.
   * 
   * @param number The number that the user entered.
   * @param result The information that is generated after processing the given number.
   */
  public void update(int number, Result result);

}
