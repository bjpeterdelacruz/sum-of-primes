package com.bpd.primes;

import java.io.IOException;
import java.util.BitSet;
import java.util.concurrent.atomic.AtomicBoolean;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.bpd.primes.PrimeFinder.Result;

/**
 * A service that will generate the representations for a number in the background on a separate thread.
 * 
 * @author BJ Peter DeLaCruz
 */
public class Generator extends Service {

  private static final String TAG = Generator.class.getCanonicalName();

  private PrimeFinder finder;
  private ResultListener listener;
  private BitSet set;

  private AtomicBoolean isSet = new AtomicBoolean(false);
  private AtomicBoolean isRunning = new AtomicBoolean(false);

  /**
   * Sets the PrimeFinder instance.
   * 
   * @param finder The PrimeFinder instance.
   */
  public void setPrimeFinder(PrimeFinder finder) {
    this.finder = finder;
  }

  /**
   * Sets the ResultListener instance.
   * 
   * @param listener The ResultListener instance.
   */
  public void setResultListener(ResultListener listener) {
    this.listener = listener;
  }

  private final IBinder binder = new LocalBinder();

  /** {@inheritDoc} */
  @Override
  public IBinder onBind(Intent intent) {
    return binder;
  }

  /**
   * Generates the representations for the given number on a separate thread. The results are sent to the listener that
   * will process them.
   * 
   * @param number
   */
  public void generate(final int number) {
    Thread thread = new Thread() {
      @Override
      public void run() {
        try {
          Result result = finder.calculate(number, getBitSet(number));
          listener.update(number, result);
        }
        catch (IOException ioe) {
          Log.e(TAG, ioe.getMessage());
          return;
        }
        catch (CancelledException ce) {
          Log.e(TAG, ce.getMessage());
          return;
        }
      }
    };
    thread.start();
  }

  /**
   * Gathers all of the prime numbers listed in the given file. If a prime number in the file is greater than half the
   * given number, the rest of the file will not be processed.
   * 
   * @param file The file to process.
   * @param number The number entered by the user.
   * @param isPrime True if the number entered by the user is prime, false otherwise.
   * @return A list of prime numbers read in from the given file.
   * @throws IOException If there was a problem processing the file.
   * @throws CancelledException If the user cancels the generation.
   */
  private BitSet getBitSet(int number) throws IOException, CancelledException {

    if (isSet.get()) {
      return set;
    }

    if (!isRunning.get()) {
      isRunning.set(true);
      new Thread(new Runnable() {

        @Override
        public void run() {
          try {
            set = PrimeFinder.getBitSet(getResources().openRawResource(R.raw.odds), finder);
          }
          catch (IOException ioe) {
            Log.e(TAG, ioe.getMessage(), ioe);
          }
          catch (CancelledException ce) {
            Log.e(TAG, ce.getMessage(), ce);
          }
          finally {
            if (set != null) {
              isSet.set(true);
            }
            isRunning.set(false);
          }
        }

      }).start();
    }

    // set = new BitSet();
    // set.set(2);

    // BitInputStream bis = null;
    // try {
    // bis = new BitInputStream(getResources().openRawResource(R.raw.odds));
    // int i = 1;
    // int val = bis.read(1); // read in 2, already set above
    // while ((val = bis.read(1)) != -1) {
    // if (val == 1) {
    // set.set(i + i + 1);
    // }
    // i++;
    // }
    // }
    // finally {
    // if (bis != null) {
    // bis.close();
    // }
    // }

    return PrimeFinder.getBitSet(getResources().openRawResource(R.raw.odds), finder, number);
  }

  /**
   * A binder that will return an instance of the Generator class.
   * 
   * @author BJ Peter DeLaCruz
   */
  public class LocalBinder extends Binder {
    /** @return An instance of the Generator class. */
    Generator getService() {
      return Generator.this;
    }
  }

}
