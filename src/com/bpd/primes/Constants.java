package com.bpd.primes;

import java.text.NumberFormat;
import java.util.Locale;
import com.bpd.primes.PrimeFinder.Result;

public final class Constants {

  /** The maximum number that the user can enter for this program. */
  public static final int LIMIT = 100000000; // 100 million

  // Actions
  static final String CALCULATION_COMPLETED_ACTION = "com.bpd.primes.CALCULATION_COMPLETED";

  // UI
  /** About. */
  public static final String ABOUT = "About";
  /** Cancel. */
  static final String CANCEL = "Cancel";
  /** Clear. */
  static final String CLEAR = "Clear";
  /** A message notifying the user that the calculation was cancelled. */
  public static final String CALCULATION_CANCELLED = "Calculation cancelled";
  /** Copy to Clipboard. */
  public static final String COPY_TO_CLIPBOARD = "Copy to Clipboard";
  /** Developer's Website. */
  public static final String DEVELOPER_WEBSITE = "Developer's Website";
  /** Help. */
  public static final String HELP = "Help";
  /** The maximum number formatted for display on the screen. */
  static final String LIMIT_STRING = NumberFormat.getInstance().format(LIMIT);
  /** Message that is displayed when user enters a number that is too large. */
  static final String NUMBER_TOO_LARGE_MESSAGE = String.format("Please enter a number less than or equal to %s.",
      LIMIT_STRING);
  /** A message notifying the user that the number entered has no representations. */
  public static final String NO_REPRESENTATIONS = "has no representations.";
  /** A message notifying the user that the number entered is too large. */
  public static final String NUMBER_TOO_LARGE = "Number Too Large";
  /** OK. */
  public static final String OK = "OK";
  static final String GENERATING_MESSAGE = "Generating representations...";

  // Error-handling
  /** Error code. */
  static final int ERROR = -1;

  /** Result object that is returned when user cancels calculation. */
  public static final Result CANCELLED_RESULT = new Result(0, "");

  // Miscellaneous
  /** The word "representation". */
  public static final String REPRESENTATION = "representation";

  /** Message for the help dialog. */
  static final String HELP_MESSAGE =
      "A positive integer can be represented by zero, one, or more sums of consecutive prime numbers."
          + " For example, 53 has two representations: 5 + 7 + 11 + 13 + 17 and 53.\n\n"
          + "41 has 3 representations, and the integer 3 only has one representation: 3.\n\n"
          + "20 has no such representations because its summands, 7 + 13 and 3 + 5 + 5 + 7,"
          + " are not consecutive prime numbers.\n\n"
          + "This program counts and displays all representations for positive integers up to and including "
          + LIMIT_STRING + ".";

  private static final int MAJOR_VERSION = 1;
  private static final int MINOR_VERSION = 3;
  /** My website URL. */
  static final String WEBSITE = "http://www.bjpeterdelacruz.com";
  /** Message for the about dialog. */
  static final String ABOUT_MESSAGE = String.format(Locale.US, "Sum of Primes (%d.%d)\nBJ Peter DeLaCruz\n%s",
      MAJOR_VERSION, MINOR_VERSION, WEBSITE);

  /** The ticker message. */
  static final String TICKER_MESSAGE = "Calculation completed.";
  /** Message that is displayed in notification drawer. */
  static final String NOTIFICATION_MESSAGE = String.format("%s Tap to view result.", TICKER_MESSAGE);

  /** Don't instantiate this class. */
  private Constants() {
  }

}
