package com.bpd.primes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import com.bpd.primes.Generator.LocalBinder;
import com.bpd.primes.PrimeFinder.Result;

/**
 * The main activity of this app, and the only activity. Contains buttons that the user presses to enter a positive
 * integer. The work of finding one or more sums of prime numbers for the number is done in a service that is NOT
 * running in a separate process. Idea for future improvement: the work should be done in a separate process so that
 * numbers greater than 100,000,000 can be supported.
 * 
 * @author BJ Peter DeLaCruz
 */
public class SumOfPrimesActivity extends Activity {

  private static final int NOTIFICATION_ID = 123456789;

  private static final String TAG = "SumOfPrimesActivity";

  private final PrimeFinder finder = new PrimeFinder();

  private Context context;

  private EditText numberEdit;
  private Button buttonClear, buttonEnter, buttonHelp;
  private ProgressBar progressBar;
  private TextView resultsText;

  /**
   * A BroadcastReceiver that will set the result code to RESULT_OK if this activity is in the foreground. If this
   * activity is not in the foreground, the BroadcastReceiver that receives the broadcast sent from the ResultListener
   * will send a notification.
   */
  private final BroadcastReceiver receiver = new BroadcastReceiver() {

    @Override
    public void onReceive(Context context, Intent intent) {
      if (!isOrderedBroadcast()) {
        return;
      }
      setResultCode(RESULT_OK);
    }

  };

  private Generator service;
  private boolean isBound;

  /** {@inheritDoc} */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sum_consecutive_primes_new);

    context = getApplicationContext();
    context.registerReceiver(receiver, new IntentFilter(Constants.CALCULATION_COMPLETED_ACTION));

    numberEdit = (EditText) findViewById(R.id.number);
    numberEdit.addTextChangedListener(new TextWatcher() {

      @Override
      public void afterTextChanged(Editable s) {
        buttonEnter.setEnabled(numberEdit.getText().toString().length() > 0);
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

    });
    progressBar = (ProgressBar) findViewById(R.id.activityIndicator);
    progressBar.setVisibility(View.INVISIBLE);
    buttonClear = (Button) findViewById(R.id.buttonClear);
    buttonEnter = (Button) findViewById(R.id.buttonEnter);
    buttonHelp = (Button) findViewById(R.id.buttonHelp);
    // buttonAbout = (Button) findViewById(R.id.buttonAbout);
    resultsText = (TextView) findViewById(R.id.results);
    registerForContextMenu(resultsText);

    buttonClear.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        numberEdit.setText("");
        resultsText.setText("");
        finder.setIsCancelled(true);
        reset();
      }

    });

    buttonEnter.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        finder.setIsCancelled(false);
        String text = numberEdit.getText().toString();
        if (text.isEmpty()) {
          Log.i(TAG, String.format("User entered empty string"));
          return;
        }
        int number;
        try {
          number = Integer.parseInt(text);
        }
        catch (NumberFormatException nfe) {
          number = Constants.ERROR;
        }
        if (number == 0) {
          Log.i(TAG, String.format("User entered 0"));
          return;
        }
        if (number == Constants.ERROR || number > Constants.LIMIT) {
          resultsText.setText(Constants.NUMBER_TOO_LARGE_MESSAGE);
          Log.i(TAG, String.format("User entered %s", text));
          return;
        }
        Log.i(TAG, String.format("User entered %d", number));
        progressBar.setVisibility(View.VISIBLE);
        buttonClear.setText(Constants.CANCEL);
        buttonEnter.setEnabled(false);
        resultsText.setText(Constants.GENERATING_MESSAGE);

        service.generate(number);
      }

    });
    buttonEnter.setEnabled(false);

    buttonHelp.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SumOfPrimesActivity.this);
        builder.setMessage(Constants.HELP_MESSAGE);
        builder.setTitle(Constants.HELP);
        AlertDialog dialog = builder.setPositiveButton(Constants.OK, null).create();
        dialog.show();
      }

    });

    /* buttonAbout.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SumOfPrimesActivity.this);
        builder.setMessage(Constants.ABOUT_MESSAGE);
        builder.setTitle(Constants.ABOUT);
        AlertDialog dialog =
            builder.setPositiveButton(Constants.OK, null)
                .setNeutralButton(Constants.DEVELOPER_WEBSITE, new android.content.DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(Constants.WEBSITE));
                    startActivity(i);
                  }
                }).create();
        dialog.setIcon(R.drawable.two);
        dialog.show();
      }

    }); */

    Intent intent = new Intent(this, Generator.class);
    startService(intent);
    bindService(intent, connection, Context.BIND_AUTO_CREATE);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    menu.add(0, v.getId(), 0, Constants.COPY_TO_CLIPBOARD);
  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {
    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
    clipboard.setText(resultsText.getText());
    return true;
  }

  /**
   * The service connection that will set up the service when a client connects to the service.
   */
  private ServiceConnection connection = new ServiceConnection() {

    /** {@inheritDoc} */
    @Override
    public void onServiceConnected(ComponentName clsasName, IBinder binder) {
      LocalBinder localBinder = (LocalBinder) binder;
      service = localBinder.getService();
      service.setPrimeFinder(finder);
      service.setResultListener(new ResultListenerImpl());
      isBound = true;
    }

    /** {@inheritDoc} */
    @Override
    public void onServiceDisconnected(ComponentName className) {
      isBound = false;
    }

  };

  /** {@inheritDoc} */
  @Override
  protected void onResume() {
    super.onResume();
    context.registerReceiver(receiver, new IntentFilter(Constants.CALCULATION_COMPLETED_ACTION));
  }

  /** {@inheritDoc} */
  @Override
  protected void onPause() {
    context.unregisterReceiver(receiver);
    super.onPause();
  }

  /** {@inheritDoc} */
  @Override
  protected void onStop() {
    super.onStop();
    if (isBound) {
      unbindService(connection);
      isBound = false;
    }
  }

  /**
   * A listener that will update the UI with information stored in the Result object that is returned from the service.
   * 
   * @author BJ Peter DeLaCruz
   */
  private class ResultListenerImpl implements ResultListener {

    /** {@inheritDoc} */
    @Override
    public void update(int number, Result result) {
      numberEdit.post(new UpdateAction(number, result));
      context.sendOrderedBroadcast(new Intent(Constants.CALCULATION_COMPLETED_ACTION), null, new BroadcastReceiver() {

        /** {@inheritDoc} */
        @Override
        public void onReceive(Context context, Intent intent) {
          if (getResultCode() == RESULT_OK) {
            return;
          }

          final Intent restartActivityIntent = new Intent(context, SumOfPrimesActivity.class);
          final PendingIntent pendingIntent =
              PendingIntent.getActivity(context, 0, restartActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
          RemoteViews mContentView = new RemoteViews(context.getPackageName(), R.layout.custom_notification);
          mContentView.setImageViewResource(R.id.image, R.drawable.two_launcher);
          mContentView.setTextViewText(R.id.text, Constants.NOTIFICATION_MESSAGE);
          Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
          Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.two_launcher);
          Notification.Builder notificationBuilder =
              new Notification.Builder(context).setSmallIcon(R.drawable.two).setLargeIcon(bitmap)
                  .setContentTitle("Sum of Primes").setContentText(Constants.NOTIFICATION_MESSAGE).setAutoCancel(true)
                  .setContentIntent(pendingIntent).setTicker(Constants.TICKER_MESSAGE).setSound(soundUri);
          NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
          manager.notify(NOTIFICATION_ID, notificationBuilder.getNotification());
        }

      }, null, 0, null, null);
    }

  }

  @Override
  public void onBackPressed() {
    if (buttonClear.getText().toString().equals(Constants.CANCEL)) {
      // If we're busy generating representations, don't destroy this activity.
      moveTaskToBack(true);
    }
    else {
      super.onBackPressed();
    }
  }

  /**
   * Updates the UI with the information stored in the given Result object.
   * 
   * @author BJ Peter DeLaCruz
   */
  private class UpdateAction implements Runnable {

    private final int number;
    private final Result result;

    /**
     * Creates a new UpdateAction instance.
     * 
     * @param number The number that the user entered.
     * @param result Contains information that is generated after processing the given number.
     */
    public UpdateAction(int number, Result result) {
      this.number = number;
      this.result = result;
    }

    /** {@inheritDoc} */
    @Override
    public void run() {
      reset();
      finder.setIsCancelled(false);
      numberEdit.setText("");

      if (result == Constants.CANCELLED_RESULT) {
        resultsText.setText(Constants.CALCULATION_CANCELLED);
      }
      else if (result.getCount() == 0) {
        resultsText.setText(String.format("%d %s", number, Constants.NO_REPRESENTATIONS));
      }
      else {
        String r = Constants.REPRESENTATION;
        String representation = result.getCount() == 1 ? r : String.format("%ss", r);
        String numReps = number + " has " + result.getCount() + " " + representation + ":\n\n";
        resultsText.setText(numReps + result.getString());
      }
    }

  }

  private void reset() {
    buttonClear.setText(Constants.CLEAR);
    buttonEnter.setEnabled(false);
    progressBar.setVisibility(View.INVISIBLE);
  }

}
