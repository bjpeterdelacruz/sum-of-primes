/*******************************************************************************
 * Copyright (C) 2012 BJ Peter DeLaCruz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.bpd.primes;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class contains methods that generate the number of representations for a given positive integer.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class PrimeFinder {

  private final AtomicBoolean isCancelled = new AtomicBoolean(false);

  /**
   * Sets the isCancelled flag.
   * 
   * @param flag True to cancel the calculation, false otherwise.
   */
  public void setIsCancelled(boolean flag) {
    isCancelled.set(flag);
  }

  /** @return True if the calculation is cancelled, false otherwise. */
  public boolean isCancelled() {
    return isCancelled.get();
  }

  /**
   * Generates a string that contains the representations for the given number given the list of prime numbers.
   * 
   * @param number The number for which to generate the representations.
   * @param primes The list of prime numbers.
   * @return A string that contains the representations for the given number.
   */
  public Result calculate(int number, BitSet set) {
    List<String> results = new ArrayList<String>();
    // int half = (number / 2) + 1;
    // for (int current = 0; current <= number; current++) {
    // if (!set.get(current)) {
    // continue;
    // }
    // if (isCancelled()) {
    // return Constants.CANCELLED_RESULT;
    // }
    // BitSet temp = new BitSet();
    // temp.set(current);
    // int sum = current;
    // for (int next = current + 1; next <= half && sum < number; next++) {
    // if (!set.get(next)) {
    // continue;
    // }
    // if (isCancelled()) {
    // return Constants.CANCELLED_RESULT;
    // }
    // sum += next;
    // temp.set(next);
    // }
    // if (sum == number) {
    // results.add(format(temp));
    // }
    // }

    for (int current = set.nextSetBit(0); current != -1; current = set.nextSetBit(current + 1)) {
      if (isCancelled()) {
        return Constants.CANCELLED_RESULT;
      }
      int index = current > 0 ? current + current + 1 : 2;
      if (index > number) {
        break;
      }
      BitSet temp = new BitSet();
      temp.set(index);
      int sum = index;
      for (int next = set.nextSetBit(current + 1); sum < number; next = set.nextSetBit(next + 1)) {
        if (isCancelled()) {
          return Constants.CANCELLED_RESULT;
        }
        index = next + next + 1;
        sum += index;
        temp.set(index);
      }
      if (sum == number) {
        results.add(format(temp));
      }
    }

    return new Result(results.size(), append(results));
  }

  public static BitSet getBitSet(InputStream is, PrimeFinder finder, Integer n) throws IOException, CancelledException {
    BitInputStream bis = null;
    BitSet set = new BitSet();
    try {
      bis = new BitInputStream(is);
      int i = 0;
      int val;
      while ((val = bis.read(1)) != -1) {
        if (n != null && i == n) {
          break;
        }
        if (finder.isCancelled()) {
          throw new CancelledException();
        }
        if (val == 1) {
          set.set(i);
        }
        i++;
      }
    }
    finally {
      if (bis != null) {
        bis.close();
      }
    }
    return set;
  }

  public static BitSet getBitSet(InputStream is, PrimeFinder finder) throws IOException, CancelledException {
    return getBitSet(is, finder, null);
  }

  /**
   * Returns a string representing the sum for the given list of numbers. For example, [2, 3, 5] -> 2 + 3 + 5.
   * 
   * @param summands The list of numbers.
   * @return A string representing the sum.
   */
  public static String format(BitSet set) {
    String temp = set.toString();
    temp = temp.replaceAll("\\{", "").replaceAll("\\}", "").replaceAll(",", " +");
    return temp;
  }

  /**
   * Returns a concatenated string of representations.
   * 
   * @param strings The list of strings representing the sum for a number.
   * @return A concatenated string of sums.
   */
  public static String append(List<String> strings) {
    StringBuffer buffer = new StringBuffer();
    for (int idx = 0; idx < strings.size(); idx++) {
      buffer.append(strings.get(idx));
      if (idx + 1 != strings.size()) {
        buffer.append("\n\n");
      }
    }
    return buffer.toString();
  }

  /**
   * Contains the number of representations for a given number and the concatenated string of representations.
   * 
   * @author BJ Peter DeLaCruz
   */
  public static class Result {
    private final int count;
    private final String string;

    /**
     * Creates a new Result instance.
     * 
     * @param count The number of representations.
     * @param string The concatenated string of representations.
     */
    public Result(int count, String string) {
      this.count = count;
      this.string = string;
    }

    /** @return The number of representations. */
    public int getCount() {
      return count;
    }

    /** @return The concatenated string of representations. */
    public String getString() {
      return string;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object object) {
      if (object == this) {
        return true;
      }
      if (!(object instanceof Result)) {
        return false;
      }
      Result r = (Result) object;
      return count == r.count && string.equals(r.string);
    }
  }

}
